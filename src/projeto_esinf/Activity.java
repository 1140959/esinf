package projeto_esinf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class Activity
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public abstract class Activity implements Comparable<Activity> ,ReadableToTree{

    /**
     * the project of this activity
     */
    private String projectId;
    
    /**
     * the activity key
     */
    private String activKey;

    /**
     * the activity type
     */
    private String activType;

    /**
     * the description of the activity
     */
    private String description;

    /**
     * the duration of the activity
     */
    private int duration;

    /**
     * the duration unit of the activity (hour, day, week, month ...)
     */
    private String durationUnit;

    /**
     * the previous activities of this activity
     */
    private List<String> activPrev;
    
    /**
     * the delay of this activity
     */
    private int delay;

    /**
     * the activity key by default
     */
    private static final String ACTIV_KEY_DEFAULT = "NA";

    /**
     * the activity type by default
     */
    private static final String ACTIV_TYPE_DEFAULT = "NA";

    /**
     * the activity's description by default
     */
    private static final String DESCRIPTION_DEFAULT = "NA";

    /**
     * the activity's duration by default
     */
    private static final int DURATION_DEFAULT = 0;

    /**
     * the activity's duration unit by default
     */
    private static final String DURATION_UNIT_DEFAULT = "hour";

    /**
     * the activity's previous activities by default
     */
    private static final List<String> ACTIV_PREV_DEFAULT = new ArrayList<>();

    /**
     * Constructor of the activity
     *
     * @param activKey Activity key
     * @param activType Activity type
     * @param description description
     * @param duration duration of the activity
     * @param durationUnit (hour, day, week, month ...)
     * @param activPrev the previous activities
     */
    public Activity(String activKey, String activType, String description, int duration, String durationUnit, List<String> activPrev) {
        this.activKey = activKey;
        this.activType = activType;
        this.description = description;
        this.duration = duration;
        this.durationUnit = durationUnit;
        this.activPrev = new ArrayList<>(activPrev);
    }

    /**
     * partial constructor for an activity
     * 
     * @param projectId
     * @param activKey
     * @param activType
     * @param delay 
     */
    public Activity(String projectId, String activKey, String activType, int delay) {
        this.projectId = projectId;
        this.activKey = activKey;
        this.activType = activType;
        this.delay = delay;
        this.description=DESCRIPTION_DEFAULT;
        this.durationUnit=DURATION_UNIT_DEFAULT;
        this.activPrev=ACTIV_PREV_DEFAULT;
        this.duration=DURATION_DEFAULT;
    }
    
    /**
     * Constructor of an Activity based of another copy
     *
     * @param copy the activity to be copied
     */
    public Activity(Activity copy) {
        this(copy.activKey, copy.activType, copy.description, copy.duration, copy.durationUnit, copy.activPrev);
    }

    /**
     * Constructor of an Activity by default
     */
    public Activity() {
        this(ACTIV_KEY_DEFAULT, ACTIV_TYPE_DEFAULT, DESCRIPTION_DEFAULT, DURATION_DEFAULT, DURATION_UNIT_DEFAULT, ACTIV_PREV_DEFAULT);
    }

    /**
     * method to access the activity key
     *
     * @return the activity key
     */
    public String getActivKey() {
        return activKey;
    }

    /**
     * method to access the activity type
     *
     * @return the activity type
     */
    public String getActivType() {
        return activType;
    }

    /**
     * method to access the activity's description
     *
     * @return the activity's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * method to access the activity's duration
     *
     * @return the activity's duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * method to access the duration unit of the activity
     *
     * @return the duration unit of the activity
     */
    public String getDurationUnit() {
        return durationUnit;
    }

    /**
     * method to access the list of previous avtivities
     *
     * @return the list of previous avtivities
     */
    public List<String> getActivPrev() {
        return new ArrayList<>(this.activPrev);
    }

    /**
     * sets the activity key of the activity
     *
     * @param activKey to change
     */
    public void setActivKey(String activKey) {
        this.activKey = activKey;
    }

    /**
     * sets the activity type of the activity
     *
     * @param activType to change
     */
    public void setActivType(String activType) {
        this.activType = activType;
    }

    /**
     * sets the activity description of the activity
     *
     * @param description to change
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * sets teh duration of the activity
     *
     * @param duration to change
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * sets the duration unit of the activity
     *
     * @param durationUnit to change
     */
    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    /**
     * sets the previous activities of the current activity
     *
     * @param activPrev to change
     */
    public void setActivPrev(List<String> activPrev) {
        this.activPrev = activPrev;
    }

    /**
     * the textual representation of the activity
     *
     * @return the textual representation of the Activity
     */
    @Override
    public String toString() {
        String ret = "Activity Key: " + this.activKey
                + "\nActivity Type: " + this.activType
                + "\nDescription: " + this.description
                + "\nDuration: " + this.duration
                + "\nDuration Unit: " + this.durationUnit
                + "\nActivity Previous: ";

        for (String activ : activPrev) {
            ret += "\n" + activ;
        }

        return ret;
    }

    /**
     * adds a previous activity to the current activity
     *
     * @param activPrev
     */
    public void addActivPrev(String activPrev) {
        this.activPrev.add(activPrev);
    }

    /**
     * adds a previous activity to the current activity given a position
     *
     * @param position to be added
     * @param activPrev to be added
     */
    public void addActivPrevAt(int position, String activPrev) {
        this.activPrev.add(position, activPrev);
    }

    /**
     * removes a given previous activity
     *
     * @param activPrev
     */
    public void removeActivPrev(String activPrev) {
        this.activPrev.remove(activPrev);
    }

    /**
     * removes a previous activity given it's position
     *
     * @param position
     */
    public void removeActivPrevAt(int position) {
        this.activPrev.remove(position);
    }

    /**
     * Compares two activities
     * 
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        Activity outroObj = (Activity) obj;

        if (this.activPrev.size() != outroObj.activPrev.size()) {
            return false;
        }

        for (int i = 0; i < this.activPrev.size(); i++) {
            if (!this.activPrev.get(i).equals(outroObj.activPrev.get(i))) {
                return false;
            }
        }
        
        return this.activKey.equals(outroObj.activKey) &&
                this.activType.equals(outroObj.activType) &&
                this.description.equals(outroObj.description) &&
                this.duration == outroObj.duration &&
                this.durationUnit.equals(outroObj.durationUnit);
    }

    /**
     * returns the id of the project of this activity
     * 
     * @return id
     */
    @Override
    public String getId() {
        return projectId;
    }

    /**
     * changes the project of this id
     * 
     * @param projectId 
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     *returns the delay of the activity 
     * 
     * @return delay
     */
    @Override
    public int getDelay() {
        return delay;
    }

    /**
     * changes the delay of an activity
     * 
     * @param delay 
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

    /**
     * returns the project id of a given activity
     * 
     * @return projectid
     */
    public String getProjectId() {
        return projectId;
    }

}
