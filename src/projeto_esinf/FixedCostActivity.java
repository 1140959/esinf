package projeto_esinf;

import java.util.List;

/**
 * Class FixedCostActivity
 * 
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class FixedCostActivity extends Activity {

    /**
     * the activity cost
     */
    private double activityCost;

    /**
     *  the activity cost by default
     */
    private static final double ACTIVITY_COST_DEFAULT = 0.0;

    public FixedCostActivity(String projectId, String activKey, String activType, int delay) {
        super(projectId, activKey, activType, delay);
        this.activityCost=ACTIVITY_COST_DEFAULT;
    }

    /**
     * Constructor of the fixed cost activity
     *
     * @param activityCost
     * @param activKey
     * @param activType
     * @param description
     * @param duration
     * @param durationUnit
     * @param activPrev
     */
    public FixedCostActivity(double activityCost, String activKey, String activType, String description, int duration, String durationUnit, List<String> activPrev) {
        super(activKey, activType, description, duration, durationUnit, activPrev);
        this.activityCost = activityCost;
    }

    /**
     * Construtor of a fixed cost activity based of another to be copied
     *
     * @param copy the VariableCostActivity to be copied
     */
    public FixedCostActivity(FixedCostActivity copy) {
        this(copy.activityCost, copy.getActivKey(), copy.getActivType(), copy.getDescription(), copy.getDuration(), copy.getDurationUnit(), copy.getActivPrev());
    }

    /**
     * Constructor a FixedCostActivity by default
     */
    public FixedCostActivity() {
        super();
        this.activityCost = FixedCostActivity.ACTIVITY_COST_DEFAULT;
    }

    /**
     *method to access the activity cost
     * 
     * @return the activity cost
     */
    public double getActivityCost() {
        return this.activityCost;
    }

    /**
     *changes the activity cost
     * 
     * @param activityCost to set
     */
    public void setActivityCost(double activityCost) {
        this.activityCost = activityCost;
    }

    /**
     *the textual representation of the fixed activity cost
     * 
     * @return the textual representation of the fixed activity cost
     */
    @Override
    public String toString() {
        return super.toString() + "\nActivity Cost: " + this.activityCost;
    }

    /**
     * 
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        
        FixedCostActivity outraFixed = (FixedCostActivity) obj;
        
        return this.activityCost == outraFixed.activityCost
                && super.equals(obj);
    }

    /**
     * compares 2 Fixed cost activity
     * 
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Activity o) {
        return this.getDelay()-o.getDelay();
    }

}
