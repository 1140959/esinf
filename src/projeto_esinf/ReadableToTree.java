package projeto_esinf;


/**
 * Interface to be implemented in the classes that can be
 * read to a tree
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public interface ReadableToTree {
    public String getId();
    public int getDelay();
}
