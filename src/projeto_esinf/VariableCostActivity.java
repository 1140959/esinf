package projeto_esinf;

import java.util.List;

/**
 * Class VariableCostActivity
 * 
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class VariableCostActivity extends Activity {

    /**
     * the total time of the VariableCostActivity
     */
    private double totalTime;

    /**
     * the total cost of the VariableCostActivity by default
     */
    private double totalCost;

    /**
     * the VariableCostActivity's total time by default
     */
    private static final double TOTAL_TIME_DEFAULT = 0.0;

    /**
     * the actvity's total cost by default
     */
    private static final double TOTAL_COST_DEFAULT = 0.0;

    /**
     * complete constructor
     * 
     * @param totalTime
     * @param totalCost
     * @param activKey
     * @param activType
     * @param description
     * @param duration
     * @param durationUnit
     * @param activPrev 
     */
    public VariableCostActivity(double totalTime,double totalCost, String activKey, String activType, String description, int duration, String durationUnit, List<String> activPrev) {
        super(activKey, activType, description, duration, durationUnit, activPrev);
        this.totalTime = totalTime;
        this.totalCost = totalCost;
    }

    /**
     * Partial constructor
     * 
     * @param projectId
     * @param activKey
     * @param activType
     * @param delay 
     */
    public VariableCostActivity(String projectId, String activKey, String activType, int delay) {
        super(projectId, activKey, activType, delay);
        this.totalCost=TOTAL_COST_DEFAULT;
        this.totalTime=TOTAL_TIME_DEFAULT;
    }

    /**
     * Constructor of a VariableCostActivity based of another to be copied
     * 
     * @param copy the VariableCostActivity to be copied
     */
    public VariableCostActivity(VariableCostActivity copy) {
        this(copy.totalTime,copy.totalCost, copy.getActivKey(), copy.getActivType(), copy.getDescription(), copy.getDuration(), copy.getDurationUnit(), copy.getActivPrev());

    }

    /**
     * Constructor of a VariableCostActivity by default
     */
    public VariableCostActivity() {
        super();
        this.totalCost=TOTAL_COST_DEFAULT;
        this.totalTime = TOTAL_TIME_DEFAULT;
    }

    /**
     * method to access the total time of the VariableCostActivity
     * 
     * @return total time
     */
    public double getTotalTime() {
        return this.totalTime;
    }

    /**
     * method to access the total cost of the activity
     *
     * @return the total cost of the activity
     */
    public double getTotalCost() {
        return totalCost;
    }

    /**
     * changes the total time of VariableCostActivity
     * 
     * @param totalTime to set
     */
    public void setTotalTime(double totalTime) {
        this.totalTime = totalTime;
    }

    /**
     * sets the total cost of the activity
     *
     * @param totalCost to change
     */
    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
    
    /**
     * the texual representation of the VariableCostActivity
     * 
     * @return the texual representation of the VariableCostActivity
     */
    @Override
    public String toString() {
        return super.toString() + "\nTotal Time: " + this.totalTime
                + "\nTotal Cost: " + this.totalCost;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        
        VariableCostActivity outroVariable = (VariableCostActivity) obj;
        
        return this.totalCost == outroVariable.totalCost 
                && this.totalTime == outroVariable.totalTime
                && super.equals(outroVariable);
    }

    /**
     * compares 2 variable cost activities
     * 
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Activity o) {
        return this.getDelay()-o.getDelay();
    }
    
}
