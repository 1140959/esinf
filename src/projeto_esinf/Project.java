package projeto_esinf;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Project
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class Project implements Comparable<Project>,ReadableToTree{

    /**
     * the id of the project
     */
    private String id;
    
    /**
     * the type of the project
     */
    private String type;
    
    /**
     * the duration of the project
     */
    private int duration;
    
    /**
     * the delay of the project
     */
    private int delay;

    /**
     * the list of activities of the project
     */
    private List<Activity> activiesList;

    /**
     * the id of the project by default
     */
    private static final String ID_DEFAULT = "N/A";
    
    /**
     * the type of the project by default
     */
    private static final String TYPE_DEFAULT = "N/A";
    
    /**
     * the duration of the project by default
     */
    private static final int DURATION_DEFAULT = 0;
    
    /**
     * the delay of the project by default
     */
    private static final int DELAY_DEFAULT =0;

    /**
     * the list of activities of the project by default
     */
    private static final List<Activity> ACTIVITIES_LIST_DEFAULT = new ArrayList<>();

    /**
     * Constructor of a project
     *
     * @param id the id of the project
     * @param activiesList the list of activities of the project
     * @param type the type of the project
     * @param duration the duration of the project
     * @param delay the delay of the project
     */ 
    public Project(String id, List<Activity> activiesList, String type,int duration,int delay) {
        this.id = id;
        this.activiesList = activiesList;
        this.type = type;
        this.duration = duration;
        this.delay =delay;
    }

    /**
     * Constructor of a another to copy
     *
     * @param copy the project to copy
     */
    public Project(Project copy) {
        this(copy.getId(), new ArrayList<>(copy.getActiviesList()),copy.getType(),copy.getDuration(),copy.getDelay());
    }

    /**
     * Constructor of a project by default
     */
    public Project() {
        this(ID_DEFAULT, ACTIVITIES_LIST_DEFAULT,TYPE_DEFAULT,DURATION_DEFAULT,DELAY_DEFAULT);
    }

    /**
     * method to access the id of the project
     *
     * @return id of the project
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * changes the id of the project
     *
     * @param id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * method to access the list of activities
     *
     * @return the activities list of the project
     */
    public List<Activity> getActiviesList() {
        return new ArrayList<>(activiesList);
    }

    /**
     * changes the activities list of the project
     *
     * @param activiesList to set
     */
    public void setActiviesList(List<Activity> activiesList) {
        this.activiesList = new ArrayList<>(activiesList);
    }

    /**
     * the textual representation of the project
     *
     * @return textual representation of the project
     */
    @Override
    public String toString() {
        String ret = "Activities List: ";

        for (Activity act : activiesList) {
            ret += "\n\n" + act.toString();
        }

        return ret;
    }

    /**
     * adds an activity to the activities list
     *
     * @param activity
     */
    public void addActiv(Activity activity) {
        this.activiesList.add(activity);
    }

    /**
     * adds an activity to the list given a position
     *
     * @param position to be added
     * @param activity to be added
     */
    public void addActivAt(int position, Activity activity) {
        this.activiesList.add(position, activity);
    }

    /**
     * removes a given activity
     *
     * @param activity
     */
    public void removeActiv(Activity activity) {
        this.activiesList.remove(activity);
    }

    /**
     * removes an activity given it's position
     *
     * @param position
     */
    public void removeActivAt(int position) {
        this.activiesList.remove(position);
    }

    /**
     * returns an activity given it's key
     * else returns null
     * 
     * @param key
     * @return the activity
     */
    public Activity getActivityByKey(String key) {
        for (Activity act : activiesList) {
            if (key.equals(act.getActivKey())) {
                return act;
            }
        }
        return null;
    }

    /**
     * tests if two projects are equal
     * 
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        
        Project outroProject = (Project) obj;
        
        if(this.activiesList.size() != outroProject.activiesList.size()) {
            return false;
        }
        
        for (int i = 0; i < this.activiesList.size(); i++) {
            if(!this.activiesList.get(i).equals(outroProject.activiesList.get(i))) {
                return false;
            }
        }
        
        return this.id == outroProject.id;
    }

    /**
     * returns the type of the project
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * changes the type of the project
     * @param type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * returns the duration of the project
     * @return duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * changes the duration of the project
     * @param duration to set
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * returns the delay of the project
     * @return delay
     */
    @Override
    public int getDelay() {
        return delay;
    }

    /**
     * changes the delay of the project
     * @param delay to set
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

    /**
     * compares 2 projects
     * 
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Project o) {
        return this.delay-o.delay;
    }

}
