package projeto_esinf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import search_tree.TreeDelays;
import search_tree.TreeDelays.Ocorrences;

/**
 * Class TreeFase3
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class TreeFase3 {

    /**
     * the projects tree
     */
    private TreeDelays<Project> projectTree;

    /**
     * the activities tree
     */
    private TreeDelays<Activity> activityTreeList;

    /**
     * Constructor that builds the trees from a List of Projects
     * 
     * @param list 
     */
    public TreeFase3(List<Project> list) {
        this.projectTree = new TreeDelays();
        this.activityTreeList = new TreeDelays();

        for (Project proj : list) {
            this.projectTree.insertActivity(proj);
            for (Activity act : proj.getActiviesList()) {
                this.activityTreeList.insertActivity(act);
            }
        }
    }

    /**
     * returns the tree of the projects
     * @return tree
     */
    public TreeDelays getProjectTree() {
        return projectTree;
    }

    /**
     * changes the tree of the projects
     * @param projectTree 
     */
    public void setProjectTree(TreeDelays projectTree) {
        this.projectTree = projectTree;
    }

    /**
     * returns the tree with all the activities
     * @return tree
     */
    public TreeDelays getActivityTreeList() {
        return activityTreeList;
    }

    /**
     * changes the tree of the activities
     * @param activityTreeList 
     */
    public void setActivityTreeList(TreeDelays activityTreeList) {
        this.activityTreeList = activityTreeList;
    }

    /**
     *Prints the projects by delay time
     * 
     * @return
     */
    public String printActivitiesByDelayTime() {
        String ret = "";
        Iterator<Ocorrences<Activity>> iterator = activityTreeList.inOrder().iterator();
        while (iterator.hasNext()) {
            Ocorrences<Activity> next = iterator.next();
            for (Activity act : next.getList()) {
                ret += act.getId() + "." + act.getActivKey()+" ";
            }
            ret += "Delay:" + next.getDelay() + "\n";
        }
        return ret.trim();
    }

    /**
     *for two projects with delay, returns the activities with delay
     * with the same type, null if the projects do not have delay
     * 
     * @param p1
     * @param p2
     * @return
     */
    public List<List<Activity>> lateActivitiesWithSameType(Project p1, Project p2) {
        if (p1.getDelay() <= 0 || p2.getDelay() <= 0) {
            return null;
        }

        List<Activity> fixedCostActivities = new ArrayList<>();
        List<Activity> variableCostActivities = new ArrayList<>();

        Iterator<Ocorrences<Activity>> iterator = activityTreeList.inOrder().iterator();
        while (iterator.hasNext()) {
            Ocorrences<Activity> next = iterator.next();
            if (next.getDelay() > 0) {
                for (Activity act : next.getList()) {
                    if (act.getProjectId().equals(p1.getId()) || act.getProjectId().equals(p2.getId())) {
                        if (act.getActivType().equals("FCA")) {
                            fixedCostActivities.add(act);
                        } else if (act.getActivType().equals("VCA")) {
                            variableCostActivities.add(act);
                        }
                    }
                }
            }
        }

        return Arrays.asList( variableCostActivities,fixedCostActivities);
    }
    
}
