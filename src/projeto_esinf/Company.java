package projeto_esinf;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class Company
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class Company {

    /**
     * the list of projects of the company
     */
    private List<Project> projectList;

    /**
     * the list of projects of the company by default
     */
    private static final List<Project> PROJECT_LIST_DEFAULT = new ArrayList<>();

    /**
     * Constructor of a company
     *
     * @param projectList
     */
    public Company(List projectList) {
        this.projectList = projectList;
    }

    /**
     * Constructor of a company based of another company
     *
     * @param copy
     */
    public Company(Company copy) {
        this(copy.getProjectList());
    }

    /**
     * Constructor of a company by default
     */
    public Company() {
        this.projectList = PROJECT_LIST_DEFAULT;
    }

    /**
     * method to access the list of projects
     *
     * @return the list of projects
     */
    public List<Project> getProjectList() {
        return new ArrayList<>(projectList);
    }

    /**
     * changes the project list of the company
     *
     * @param projectList to set
     */
    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    /**
     * returns all the projects of the company
     *
     * @return the textual representation of all the preojects
     */
    @Override
    public String toString() {
        String ret = "List of projects:";
        for (Project proj : projectList) {
            ret += "\n" + proj.getId();
        }

        return ret;
    }

    /**
     * adds a project to the company
     *
     * @param project
     */
    public void addProject(Project project) {
        this.projectList.add(project);
    }

    /**
     * adds a project to the company given it's position
     *
     * @param position to be added
     * @param project to be added
     */
    public void addProject(int position, Project project) {
        this.projectList.add(position, project);
    }

    /**
     * removes a given project
     *
     * @param project
     */
    public void removeProject(Project project) {
        this.projectList.remove(project);
    }

    /**
     * removes a project given it's position
     *
     * @param position
     */
    public void removeProject(int position) {
        this.projectList.remove(position);
    }

    /**
     * Compares if 2 companies are equal
     * 
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        
        Company outroCompany = (Company) obj;
        
        if(this.projectList.size() != outroCompany.projectList.size()) {
            return false;
        }
        
        for (int i = 0; i < this.projectList.size(); i++) {
            if(!this.projectList.get(i).equals(outroCompany.projectList.get(i))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.projectList);
        return hash;
    }
}
