package search_tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import projeto_esinf.Activity;
import projeto_esinf.ReadableToTree;
import search_tree.TreeDelays.Ocorrences;

/**
 * Class TreeDelays (A tree of objects that implement the interface ReadableToTree )
 * needs to have a delay and an id
 * 
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 * @param <E>
 */
public class TreeDelays<E extends ReadableToTree> extends AVL<Ocorrences<E>> {

    /**
     * empty constructor
     */
    public TreeDelays() {
    }

    /**
     * inserts a ReadableToTree object to the tree
     * @param act 
     */
    public void insertActivity(E act) {
        insert(new Ocorrences(Arrays.asList(act), act.getDelay()));
    }

    /**
     * inserts a node in the tree
     * @param element 
     */
    @Override
    public void insert(Ocorrences<E> element) {
        root = insert(element, root);
    }

    /**
     * recursive method to be called by insertActivity
     * @param element 
     */
    private Node<Ocorrences<E>> insert(Ocorrences<E> element, Node<Ocorrences<E>> node) {
        if (node == null) {
            return new Node<>(element, null, null);
        }

        if (node.getElement().getDelay() == element.getDelay()) {
            for (E act : element.getList()) {
                node.getElement().incOcorrences(act);
            }
        } else {
            if (node.getElement().compareTo(element) > 0) {
                node.setLeft(insert(element, node.getLeft()));
            } else {
                node.setRight(insert(element, node.getRight()));
            }
        }
        return balanceNode(node);
    }

    // inner nested class Ocorrences
    public static class Ocorrences<E extends ReadableToTree> implements Comparable<Ocorrences<E>> {

        /**
         * projects of the ocorrence
         */
        private List<E> projects;
        
        /**
         * delay of the projects
         */
        private int delay;

        /**
         * empty constructor
         */
        public Ocorrences() {
            this.projects = new ArrayList<>();
            this.delay = 0;
        }

        /**
         * complete constructor
         * @param project
         * @param delay 
         */
        public Ocorrences(List<E> project, int delay) {
            this.projects = new ArrayList<>(project);
            this.delay = delay;
        }

        /**
         * compares 2 ocorrencies by delay
         * 
         * @param o
         * @return 
         */
        @Override
        public int compareTo(Ocorrences<E> o) {
            return this.delay - o.delay;
        }

        /**
         * increases the ocorrences 
         * 
         * @param proj 
         */
        public void incOcorrences(E proj) {
            this.projects.add(proj);
        }

        /**
         * to string method of ocorrences
         * @return 
         */
        @Override
        public String toString() {
            String ret = "<";
            for (E project : projects) {
                if (project instanceof Activity) {
                    Activity aux = (Activity) project;
                    ret += aux.getProjectId() + "." + aux.getActivKey() + " ";
                } else {
                    ret += project.getId() + " ";
                }
            }
            ret += ">:" + delay;
            return ret;
        }

        /**
         * returns all of the objects of the ocorrence in a String format
         * @return 
         */
        public String getOcorrences() {
            String ret = "";
            for (E project : projects) {
                if (project instanceof Activity) {
                    Activity aux = (Activity) project;
                    ret += aux.getProjectId() + "." + aux.getActivKey() + " ";
                } else {
                    ret += project.getId() + " ";
                }
            }
            return ret;
        }

        /**
         * returns the list of objects
         * @return 
         */
        public List<E> getList() {
            return projects;
        }

        /**
         * changes the list of objects of this ocorrence
         * @param projects 
         */
        public void setList(List<E> projects) {
            this.projects = projects;
        }

        /**
         * returns the delay
         * @return 
         */
        public int getDelay() {
            return delay;
        }

        /**
         * changes the delay
         * @param delay 
         */
        public void setDelay(int delay) {
            this.delay = delay;
        }

    }
}
