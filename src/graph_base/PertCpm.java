package graph_base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import projeto_esinf.Activity;
import projeto_esinf.FixedCostActivity;
import projeto_esinf.Project;
import projeto_esinf.VariableCostActivity;

/**
 * Class that represents a PertCpm graph of Activities
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class PertCpm extends Graph<Activity, String> {

    /**
     * the earliest start of each vertice of the graph
     */
    private int[] es;

    /**
     * the earliest finish of each vertice of the graph
     */
    private int[] ef;

    /**
     * the latest start of each vertice of the graph
     */
    private int[] ls;

    /**
     * the latest finhish of each vertice of the graph
     */
    private int[] lf;

    /**
     * the slack of each vertice of the graph
     */
    private int[] slack;

    /**
     * the empty constructor of a pertcpm graph
     */
    public PertCpm() {
        super(true);
    }

    /**
     * the constructor of a pertcpm graph given a project
     *
     * @param project
     */
    public PertCpm(Project project) {
        super(true);

        final Activity start = new FixedCostActivity(0, "START", "", "", 0, "", new ArrayList<>());
        final Activity finish = new FixedCostActivity(0, "FINISH", "", "", 0, "", new ArrayList<>());

        //insert vertex
        this.insertVertex(start);
        for (Activity act : project.getActiviesList()) {
            this.insertVertex(act);
        }
        this.insertVertex(finish);

        // insert edges
        for (Activity act : project.getActiviesList()) {

            for (String prec : act.getActivPrev()) {
                Activity orig = project.getActivityByKey(prec);
                if (orig != null && !orig.equals(act)) {
                    this.insertEdge(orig, act, "Edge" + this.numEdges(), 0);
                }
            }
        }

        // insert edges a fim e start
        connectStartFinish();

        // calculates ES EF LS LF SLACK
        calculateEarliestSF();
        calculateLastestSF();
        calculateSlack();
    }

    /**
     * private method to be used in the constructor that connects the start and
     * finish activities to the rest of the graph
     *
     */
    private void connectStartFinish() {
        Activity start = getVertex(0).getElement();
        Activity finish = getVertex(numVertices() - 1).getElement();

        for (Vertex<Activity, String> vert : this.vertices()) {

            if (!vert.getElement().equals(start) && !vert.getElement().equals(finish)) {
                if (this.inDegree(vert) == 0) {
                    this.insertEdge(start, vert.getElement(), "Edge" + this.numEdges(), 0);
                }

                if (this.outDegree(vert) == 0) {
                    this.insertEdge(vert.getElement(), finish, "Edge" + this.numEdges(), 0);
                }
            }
        }
    }

    /**
     * method that calculates the earliest start and earlist finish of all of
     * the graph's vertices used in the constructor and in case of insertion or
     * removal of a vertex
     */
    private void calculateEarliestSF() {
        es = new int[this.numVertices()];
        ef = new int[this.numVertices()];
        es[0] = 0;
        ef[0] = 0;

        for (int i = 1; i < this.numVertices(); i++) {
            int prev = Integer.MIN_VALUE;
            Vertex<Activity, String> orig = this.getVertex(i);
            for (Edge<Activity, String> edge : this.incomingEdges(orig)) {
                Vertex<Activity, String> dest = edge.getEndpoints()[0];
                if (prev < ef[this.getVertex(dest.getElement()).getKey()]) {
                    prev = ef[this.getVertex(dest.getElement()).getKey()];
                }
            }
            ef[i] = prev + orig.getElement().getDuration();
            es[i] = prev;
        }
    }

    /**
     * method that calculates the latest start and latest finish of all of the
     * graph's vertices used in the constructor and in case of insertion or
     * removal of a vertex
     */
    private void calculateLastestSF() {
        ls = new int[this.numVertices()];
        lf = new int[this.numVertices()];
        ls[this.numVertices() - 1] = es[this.numVertices() - 1];
        lf[this.numVertices() - 1] = ef[this.numVertices() - 1];

        for (int i = this.numVertices() - 2; i > 0; i--) {
            int prev = Integer.MAX_VALUE;
            Vertex<Activity, String> orig = this.getVertex(i);

            for (Edge<Activity, String> edge : this.outgoingEdges(orig)) {
                Vertex<Activity, String> dest = edge.getEndpoints()[1];
                if (prev > ls[this.getVertex(dest.getElement()).getKey()]) {
                    prev = ls[this.getVertex(dest.getElement()).getKey()];
                }
            }
            ls[i] = prev - orig.getElement().getDuration();
            lf[i] = prev;
        }
    }

    /**
     * method that calculates the slack of all of the graph's vertices used in
     * the constructor and in case of insertion or removal of a vertex
     */
    private void calculateSlack() {
        slack = new int[this.numVertices()];
        for (int i = 0; i < ef.length; i++) {
            slack[i] = lf[i] - ef[i];
        }
    }

    /**
     * returns the list of activities by order of completion using it's earliest
     * finishes
     *
     * @return the list of activites
     */
    public List<Activity> getActivitiesCompletionOrderByEF() {
        List<Activity> ordered = new ArrayList<>();
        int size[] = new int[numVertices() - 2];

        //preencher arrays
        for (int i = 1; i < numVertices() - 1; i++) {
            ordered.add(getVertex(i).getElement());
            size[i - 1] = ef[i];
        }

        return ascendingSort(ordered, size);
    }

    /**
     * returns the list of activities by order of completion using it's latest
     * finishes
     *
     * @return the list of activites
     */
    public List<Activity> getActivitiesCompletionOrderByLF() {

        List<Activity> ordered = new ArrayList<>();
        int size[] = new int[numVertices() - 2];

        //preencher arrays
        for (int i = 1; i < numVertices() - 1; i++) {
            ordered.add(getVertex(i).getElement());
            size[i - 1] = lf[i];
        }

        return ascendingSort(ordered, size);
    }

    /**
     * method that sorts in ascending order to be used in
     * getActivitiesCompletionOrderByEF() getActivitiesCompletionOrderByLF()
     *
     * @param list to be ordered
     * @param array with each positions size
     * @return
     */
    private List<Activity> ascendingSort(List<Activity> ordered, int size[]) {
        //bubble sort
        int temp;
        for (int i = 0; i < size.length; i++) {

            for (int j = 1; j < size.length - i; j++) {
                if (size[j - 1] > size[j]) {
                    temp = size[j - 1];
                    size[j - 1] = size[j];
                    size[j] = temp;
                    Collections.swap(ordered, j - 1, j);
                }
            }
        }
        return ordered;
    }

    /**
     * returns the activity's earliest start -1 if activity does not exist
     *
     * @param a the activity
     * @return es
     */
    public int getES(Activity a) {
        if (this.getActivities().contains(a)) {
            return es[getVertex(a).getKey()];
        }
        return -1;
    }

    /**
     * returns the activity's earliest finish -1 if activity does not exist
     *
     * @param a the activity
     * @return ef
     */
    public int getEF(Activity a) {
        if (this.getActivities().contains(a)) {
            return ef[getVertex(a).getKey()];
        }
        return -1;
    }

    /**
     * returns the activity's latest start -1 if activity does not exist
     *
     * @param a the activity
     * @return ls
     */
    public int getLS(Activity a) {
        if (this.getActivities().contains(a)) {
            return ls[getVertex(a).getKey()];
        }
        return -1;
    }

    /**
     * returns the activity's latest finish -1 if activity does not exist
     *
     * @param a the activity
     * @return lf
     */
    public int getLF(Activity a) {
        if (this.getActivities().contains(a)) {
            return lf[getVertex(a).getKey()];
        }
        return -1;
    }

    /**
     * returns the activity's slack -1 if activity does not exist
     *
     * @param a the activity
     * @return slack
     */
    public int getSLACK(Activity a) {
        if (this.getActivities().contains(a)) {
            return slack[getVertex(a).getKey()];
        }
        return -1;
    }

    /**
     * returns an array with the es for all of the graph's activities
     *
     * @return array of Es
     */
    public int[] getEsArray() {
        return es;
    }

    /**
     * returns an array with the ef for all of the graph's activities
     *
     * @return aray of Ef
     */
    public int[] getEfArray() {
        return ef;
    }

    /**
     * returns an array with the ls for all of the graph's activities
     *
     * @return array of Ls
     */
    public int[] getLsArray() {
        return ls;
    }

    /**
     * returns an array with the es for all of the graph's activities
     *
     * @return array of Lf
     */
    public int[] getLfArray() {
        return lf;
    }

    /**
     * returns an array with the slack for all of the graph's activities
     *
     * @return array of slack
     */
    public int[] getSlackArray() {
        return slack;
    }

    /**
     * returns the cost of an activity
     *
     * @param a the activity
     * @return the cost
     */
    public String getCost(Activity a) {
        if (a instanceof VariableCostActivity) {
            VariableCostActivity act = (VariableCostActivity) a;
            return "VCA " + act.getTotalCost() * act.getTotalTime() + " €";
        } else if (a instanceof FixedCostActivity) {
            FixedCostActivity act = (FixedCostActivity) a;
            return "FCA " + act.getActivityCost() + " €";
        }
        return "Activity não é valida";
    }

    /**
     * returns a textual representation of the ES EF LS LF SLACK of each
     * activity in the graph
     *
     * @return the data
     */
    public String printData() {
        String ret = "";
        for (Vertex<Activity, String> vert : vertices()) {
            Activity act = vert.getElement();
            ret += "\n" + act.getActivKey()
                    + "\nES=" + getES(act) + " EF= " + getEF(act) + " LS= " + getLS(act) + " LF= " + getLF(act) + " Slack= " + getSLACK(act);
        }
        ret += "\n";
        return ret;
    }

    /**
     * return all the critical paths of the graph (the longest paths)
     *
     * @return list of paths
     */
    public List<Deque<Activity>> getCriticalPaths() {
        List<Deque<Activity>> allPaths = new ArrayList<>();
        List<Integer> sizes = new ArrayList<>();
        for (Path path : getAllPaths()) {
            allPaths.add(path.getPath());
            sizes.add(path.getSize());
        }

        List<Deque<Activity>> critical = new ArrayList<>();
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < sizes.size(); i++) {
            if (sizes.get(i) >= max) {
                if (sizes.get(i) > max) {
                    max = sizes.get(i);
                    critical.clear();
                }
                critical.add(allPaths.get(i));
            }
        }
        return critical;
    }

    /**
     * returns a list of Paths with all the paths between the vertices start and
     * finish of the graph with their respective size
     *
     * @return a list of paths
     */
    public Deque<Path> getAllPaths() {
        Activity start = getVertex(0).getElement();
        Activity finish = getVertex(numVertices() - 1).getElement();

        ArrayList<Deque<Activity>> paths = GraphAlgorithms.allPaths(this, start, finish);
        Deque<Path> allPaths = new LinkedList<>();
        for (Deque<Activity> path : paths) {
            int size = 0;
            for (Activity act : path) {
                size += act.getDuration();
            }
            allPaths.add(new Path(path, size));
        }
        return allPaths;
    }

    /**
     * method that tests if two graphs are equals
     *
     * @param oth the other to be compared
     * @return success of the test
     */
    @Override
    public boolean equals(Object oth) {
        if (!super.equals(oth)) {
            return false;
        }
        PertCpm other = (PertCpm) oth;
        return Arrays.equals(es, other.getEsArray()) && Arrays.equals(ef, other.getEfArray())
                && Arrays.equals(ls, other.getLsArray()) && Arrays.equals(lf, other.getLfArray())
                && Arrays.equals(slack, other.getSlackArray());
    }
    
    /**
     * returns a list of all activities in the graph
     * 
     * @return list of activities
     */
    public List<Activity> getActivities(){
        List<Activity> list = new ArrayList<>();
        for (Vertex<Activity,String> vert : this.vertices()) {
            list.add(vert.getElement());
        }
        return list;
    }

    /*---------------- start of an inner class------------*/
    /**
     * inner class to represent a path with it's course and size to be used in
     * all paths
     *
     */
    public static class Path {

        /**
         * the path of Path
         */
        private Deque<Activity> path;

        /**
         * the size of the Path
         */
        private int size;

        /**
         * complete constructor for a path
         *
         * @param paths
         * @param size
         */
        private Path(Deque<Activity> paths, int size) {
            this.path = paths;
            this.size = size;
        }

        /**
         * empty constructor for a path
         */
        private Path() {

        }

        /**
         * returns the path
         *
         * @return list of activities
         */
        public Deque<Activity> getPath() {
            return path;
        }

        /**
         * set a path
         *
         * @param path to be set
         */
        public void setPath(Deque<Activity> path) {
            this.path = path;
        }

        /**
         * returns the size of the path
         *
         * @return size of the path
         */
        public int getSize() {
            return size;
        }

        /**
         * set the path's size
         *
         * @param size to be set
         */
        public void setSize(int size) {
            this.size = size;
        }
    }

    /*---------------- end of an inner class------------*/
}
