package file_utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import projeto_esinf.Activity;
import projeto_esinf.FixedCostActivity;
import projeto_esinf.Project;
import projeto_esinf.VariableCostActivity;

/**
 * Class ReadMultipleProject
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class ReadMultipleProject {

    /**
     * Variable that limits the types of activity that the file can contain
     */
    private static final String[] NOME_VAR = {"VCA", "FCA"};

    /**
     * id e Tipo
     */
    private static final int PROJECT_DATA = 2;

    /**
     * Method that reads multiple activities from a file
     *
     * @param file
     * @return
     * @throws FileNotFoundException
     */
    public static List<Project> readFile(String file) throws FileNotFoundException {
        List<Project> projects = new ArrayList<>();
        try (Scanner ler = new Scanner(new File(file))) {
            Project p = new Project();
            int delay = 0;
            while (ler.hasNextLine()) {
                String linha = ler.nextLine();
                String[] split = linha.split(",");

                if (split.length == PROJECT_DATA) {
                    delay = 0;
                    p.setActiviesList(new ArrayList<>());
                    p.setId(split[0]);
                    p.setType(split[1]);

                } else if (linha.isEmpty()) {
                    p.setDelay(delay);
                    projects.add(new Project(p));

                } else {
                    Activity act = readLine(split);
                    delay += act.getDelay();
                    p.addActiv(act);
                }
            }
            p.setDelay(delay);
            projects.add(new Project(p));
        }
        return projects;
    }

    /**
     * Method that returns an activity from a line
     *
     * @param lineVec
     * @return Activity
     */
    private static Activity readLine(String[] lineVec) {
        Activity activity = null;
        
        if (lineVec[1].equals(NOME_VAR[0])) {
                    activity = new VariableCostActivity();
                    ((VariableCostActivity) activity).setTotalTime(Double.parseDouble(lineVec[6]));
                    ((VariableCostActivity) activity).setTotalCost(Double.parseDouble(lineVec[7]));
                    for (int i = 8; i < lineVec.length; i++) {
                        activity.addActivPrev(lineVec[i]);
                    }
                } else {
                    activity = new FixedCostActivity();
                    ((FixedCostActivity) activity).setActivityCost(Double.parseDouble(lineVec[6]));
                    for (int i = 7; i < lineVec.length; i++) {
                        activity.addActivPrev(lineVec[i]);
                    }
                }

                activity.setActivKey(lineVec[0]);
                activity.setActivType(lineVec[1]);
                activity.setDescription(lineVec[2]);
                activity.setDelay(Integer.parseInt(lineVec[2]));
                activity.setDuration(Integer.parseInt(lineVec[4]));
                activity.setDurationUnit(lineVec[5]);

        return activity;
    }
}
