package file_utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import projeto_esinf.Activity;
import projeto_esinf.FixedCostActivity;
import projeto_esinf.VariableCostActivity;

/**
 * Class ReadFile
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class ReadSingleProject {

    /**
     * Variable that limits the minimum size that must contain each row
     */
    private static final int NUM_MIN = 6;

    /**
     * Variable that limits the types of activity that the file can contain
     */
    private static final String[] NOME_VAR = {"VCA", "FCA"};

    /**
     * Reads the activity from file and create a lits of activity
     *
     * @param file with the acitivity
     * @return list of activity
     * @throws Exception new exception if the file not found or corrupt
     */
    public static List<Activity> read(String file) throws Exception {
        String lineVec[];

        Activity activity = null;
        ArrayList<Activity> activityList = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));

        try {
            String line;
            while ((line = br.readLine()) != null) {
                lineVec = line.split(",");

                //System.out.println(lineVec[0]);
                if (lineVec[1].equals(NOME_VAR[0])) {
                    activity = new VariableCostActivity();
                    ((VariableCostActivity) activity).setTotalTime(Double.parseDouble(lineVec[5]));
                    ((VariableCostActivity) activity).setTotalCost(Double.parseDouble(lineVec[6]));
                    for (int i = 7; i < lineVec.length; i++) {
                        activity.addActivPrev(lineVec[i]);
                    }
                } else {
                    activity = new FixedCostActivity();
                    ((FixedCostActivity) activity).setActivityCost(Double.parseDouble(lineVec[5]));
                    for (int i = 6; i < lineVec.length; i++) {
                        activity.addActivPrev(lineVec[i]);
                    }
                }

                activity.setActivKey(lineVec[0]);
                activity.setActivType(lineVec[1]);
                activity.setDescription(lineVec[2]);
                activity.setDuration(Integer.parseInt(lineVec[3]));
                activity.setDurationUnit(lineVec[4]);

                activityList.add(activity);
            }
        } catch (FileNotFoundException ex) {
            throw new Exception("Ficheiro não encontrado!");
        } catch (IOException ex) {
            throw new Exception("Erro critico!");
        } finally {
            br.close();
        }

        return activityList;
    }

}
