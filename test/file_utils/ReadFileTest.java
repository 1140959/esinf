/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file_utils;

import file_utils.ReadSingleProject;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import projeto_esinf.Activity;
import projeto_esinf.FixedCostActivity;
import projeto_esinf.VariableCostActivity;
import static org.junit.Assert.*;

/**
 *@author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class ReadFileTest {

    public ReadFileTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of read method, of class ReadFile.
     */
    @Test
    public void testRead() throws Exception {
        System.out.println("read");

        VariableCostActivity activityVCA = new VariableCostActivity();
        activityVCA.setActivKey("A");
        activityVCA.setActivType("VCA");
        activityVCA.setDescription("High level analysis");
        activityVCA.setDuration(1);
        activityVCA.setDurationUnit("week");
        activityVCA.setTotalTime(30);
        activityVCA.setTotalCost(112);

        FixedCostActivity activityFCA = new FixedCostActivity();
        activityFCA.setActivKey("B");
        activityFCA.setActivType("FCA");
        activityFCA.setDescription("Order Hardware platform");
        activityFCA.setDuration(4);
        activityFCA.setDurationUnit("week");
        activityFCA.setActivityCost(2500);

        List<Activity> lista = new ArrayList<>();
        lista.add(activityVCA);
        lista.add(activityFCA);
        
        String file = "fileTest.txt";
        ReadSingleProject instance = new ReadSingleProject();
        List<Activity> result = instance.read(file);

        assertEquals(lista, result);
    }

}
