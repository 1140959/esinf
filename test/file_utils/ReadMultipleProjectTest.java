/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file_utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import projeto_esinf.Activity;
import projeto_esinf.Project;

/**
 *
 * @author David
 */
public class ReadMultipleProjectTest {
    
    public ReadMultipleProjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readFile method, of class ReadMultipleProject.
     */
    @Test
    public void testReadFile() throws Exception {
        System.out.println("readFile");
        List<Project> result = new ArrayList<>(ReadMultipleProject.readFile("projects.txt"));
        List<String> proj1 = Arrays.asList("A","B","C","D","E","F","G","H","I","J");
        List<Integer>delay1 = Arrays.asList(8,4,2,6,3,70,12,15,30,25);
        List<String> proj2 = Arrays.asList("K","L","M","N","O","P","Q","R","S","T");
        List<Integer>delay2 = Arrays.asList(4,28,6,45,5,7,5,6,5,16);
        List<String> proj3 = Arrays.asList("A","B","C","D","E","F","G","H","I","J");
        List<Integer>delay3 = Arrays.asList(0,7,5,85,45,12,369,5,7,58);
        List<String> proj4 = Arrays.asList("A","B","C","D","E","F","G","H","I","J");
        List<Integer>delay4 = Arrays.asList(0,7,5,85,45,12,369,5,7,58);
        
        assertEquals(result.size(), 4);
        
        assertEquals(result.get(0).getType(), "Empresarial");
        assertEquals(result.get(1).getType(), "Lapr");
        assertEquals(result.get(2).getType(), "Java");
        assertEquals(result.get(3).getType(), "Java");
        
        assertEquals(result.get(0).getId(), "Project1");
        assertEquals(result.get(1).getId(), "Project2");
        assertEquals(result.get(2).getId(), "Project3");
        assertEquals(result.get(3).getId(), "Project4");
        
        int i=0;
        for (Activity act : result.get(0).getActiviesList()) {
            assertEquals(act.getActivKey(), proj1.get(i));
            assertEquals(act.getDelay(), (int)delay1.get(i));
            i++;
        }
        assertEquals(proj1.size(), i);
        assertEquals(delay1.size(), i);
        
        i=0;
        for (Activity act : result.get(1).getActiviesList()) {
            assertEquals(act.getActivKey(), proj2.get(i));
            assertEquals(act.getDelay(), (int)delay2.get(i));
            i++;
        }
        assertEquals(proj2.size(), i);
        assertEquals(delay2.size(), i);
        
        i=0;
        for (Activity act : result.get(2).getActiviesList()) {
            assertEquals(act.getActivKey(), proj3.get(i));
            assertEquals(act.getDelay(), (int)delay3.get(i));
           i++;
        }
        assertEquals(proj3.size(), i);
        assertEquals(delay3.size(), i);
        
        i=0;
        for (Activity act : result.get(3).getActiviesList()) {
            assertEquals(act.getActivKey(), proj4.get(i));
            assertEquals(act.getDelay(), (int)delay4.get(i));
            i++;
        }
        assertEquals(proj4.size(), i);
        assertEquals(delay4.size(), i);
        
        assertEquals(result.get(0).getDelay(), 175);
        assertEquals(result.get(1).getDelay(), 127);
        assertEquals(result.get(2).getDelay(), 593);
        assertEquals(result.get(3).getDelay(), 593);
    }
    
}
