package graphbase;

import graph_base.Vertex;
import graph_base.PertCpm;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import projeto_esinf.Activity;
import projeto_esinf.FixedCostActivity;
import projeto_esinf.Project;
import projeto_esinf.VariableCostActivity;

/**
 *
 * @author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class PertCpmTest {

    private static PertCpm createGraph() {
        List<String> prev = new ArrayList<>();
        List<Activity> activities = new ArrayList();
        activities.add(new VariableCostActivity(112,30, "A", "VCA", "", 1, "", new ArrayList<>()));
        activities.add(new FixedCostActivity(2500, "B", "FCA", "", 4, "", new ArrayList<>()));

        prev.clear();
        prev.add("B");
        activities.add(new FixedCostActivity(1250, "C", "VCA", "", 2, "", prev));

        prev.clear();
        prev.add("A");
        activities.add(new VariableCostActivity(162,30, "D", "VCA", "", 3, "", prev));

        prev.clear();
        prev.add("D");
        activities.add(new VariableCostActivity(108,30, "E", "VCA", "", 2, "", prev));

        prev.clear();
        prev.add("C");
        prev.add("D");
        activities.add(new VariableCostActivity(108,20, "F", "VCA", "", 4, "", prev));

        prev.clear();
        prev.add("E");
        prev.add("F");
        activities.add(new VariableCostActivity(54,20, "G", "VCA", "", 3, "", prev));

        prev.clear();
        prev.add("F");
        activities.add(new VariableCostActivity(54,30, "H", "VCA", "", 2, "", prev));

        prev.clear();
        prev.add("G");
        activities.add(new VariableCostActivity(27,30, "I", "VCA", "", 1, "", prev));

        prev.clear();
        prev.add("G");
        activities.add(new FixedCostActivity(550, "J", "FCA", "", 1, "", prev));

        prev.clear();
        prev.add("G");
        activities.add(new FixedCostActivity(750, "K", "FCA", "", 1, "", prev));

        prev.clear();
        prev.add("H");
        prev.add("I");
        prev.add("K");
        activities.add(new FixedCostActivity(1500, "L", "FCA", "", 2, "", prev));

        Project project = new Project("1", activities,"N/A",0,0);

        return new PertCpm(project);
    }

    public PertCpmTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getActivitiesCompletionOrderByEF method, of class PertCpm.
     */
    @Test
    public void testGetActivitiesCompletionOrderByEF() {
        System.out.println("getActivitiesCompletionOrderByEF");
        PertCpm graph = createGraph();
        List<Activity> list = graph.getActivitiesCompletionOrderByEF();
        assertTrue("Size should be 12", list.size() == 12);
        assertTrue("First activity should be A", list.get(0).getActivKey().equals("A"));
        assertTrue("First activity should be B", list.get(1).getActivKey().equals("B"));
        assertTrue("First activity should be D", list.get(2).getActivKey().equals("D"));
        assertTrue("First activity should be C", list.get(3).getActivKey().equals("C"));
        assertTrue("First activity should be E", list.get(4).getActivKey().equals("E"));
        assertTrue("First activity should be F", list.get(5).getActivKey().equals("F"));
        assertTrue("First activity should be H", list.get(6).getActivKey().equals("H"));
        assertTrue("First activity should be G", list.get(7).getActivKey().equals("G"));
        assertTrue("First activity should be I", list.get(8).getActivKey().equals("I"));
        assertTrue("First activity should be J", list.get(9).getActivKey().equals("J"));
        assertTrue("First activity should be K", list.get(10).getActivKey().equals("K"));
        assertTrue("First activity should be L", list.get(11).getActivKey().equals("L"));
    }

    /**
     * Test of getActivitiesCompletionOrderByLF method, of class PertCpm.
     */
    @Test
    public void testGetActivitiesCompletionOrderByLF() {
        System.out.println("getActivitiesCompletionOrderByLF");
        PertCpm graph = createGraph();
        List<Activity> list = graph.getActivitiesCompletionOrderByLF();
        assertTrue("Size should be 12", list.size() == 12);
        assertTrue("First activity should be A", list.get(0).getActivKey().equals("A"));
        assertTrue("First activity should be B", list.get(1).getActivKey().equals("B"));
        assertTrue("First activity should be C", list.get(2).getActivKey().equals("C"));
        assertTrue("First activity should be D", list.get(3).getActivKey().equals("D"));
        assertTrue("First activity should be E", list.get(4).getActivKey().equals("E"));
        assertTrue("First activity should be F", list.get(5).getActivKey().equals("F"));
        assertTrue("First activity should be G", list.get(6).getActivKey().equals("G"));
        assertTrue("First activity should be H", list.get(7).getActivKey().equals("H"));
        assertTrue("First activity should be I", list.get(8).getActivKey().equals("I"));
        assertTrue("First activity should be K", list.get(9).getActivKey().equals("K"));
        assertTrue("First activity should be J", list.get(10).getActivKey().equals("J"));
        assertTrue("First activity should be L", list.get(11).getActivKey().equals("L"));
    }

    /**
     * Test of getES method, of class PertCpm.
     */
    @Test
    public void testGetES() {
        System.out.println("getES");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("ES of START should be 0", graph.getES(iterator.next().getElement()) == 0);
        assertTrue("ES of A should be 0", graph.getES(iterator.next().getElement()) == 0);
        assertTrue("ES of B should be 0", graph.getES(iterator.next().getElement()) == 0);
        assertTrue("ES of C should be 4", graph.getES(iterator.next().getElement()) == 4);
        assertTrue("ES of D should be 1", graph.getES(iterator.next().getElement()) == 1);
        assertTrue("ES of E should be 4", graph.getES(iterator.next().getElement()) == 4);
        assertTrue("ES of F should be 6", graph.getES(iterator.next().getElement()) == 6);
        assertTrue("ES of G should be 10", graph.getES(iterator.next().getElement()) == 10);
        assertTrue("ES of H should be 10", graph.getES(iterator.next().getElement()) == 10);
        assertTrue("ES of I should be 13", graph.getES(iterator.next().getElement()) == 13);
        assertTrue("ES of J should be 13", graph.getES(iterator.next().getElement()) == 13);
        assertTrue("ES of K should be 13", graph.getES(iterator.next().getElement()) == 13);
        assertTrue("ES of L should be 14", graph.getES(iterator.next().getElement()) == 14);
        assertTrue("ES of FINISH should be 16", graph.getES(iterator.next().getElement()) == 16);
    }

    /**
     * Test of getEF method, of class PertCpm.
     */
    @Test
    public void testGetEF() {
        System.out.println("getEF");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("EF of START should be 0", graph.getEF(iterator.next().getElement()) == 0);
        assertTrue("EF of A should be 1", graph.getEF(iterator.next().getElement()) == 1);
        assertTrue("EF of B should be 4", graph.getEF(iterator.next().getElement()) == 4);
        assertTrue("EF of C should be 6", graph.getEF(iterator.next().getElement()) == 6);
        assertTrue("EF of D should be 4", graph.getEF(iterator.next().getElement()) == 4);
        assertTrue("EF of E should be 6", graph.getEF(iterator.next().getElement()) == 6);
        assertTrue("EF of F should be 10", graph.getEF(iterator.next().getElement()) == 10);
        assertTrue("EF of G should be 13", graph.getEF(iterator.next().getElement()) == 13);
        assertTrue("EF of H should be 12", graph.getEF(iterator.next().getElement()) == 12);
        assertTrue("EF of I should be 14", graph.getEF(iterator.next().getElement()) == 14);
        assertTrue("EF of J should be 14", graph.getEF(iterator.next().getElement()) == 14);
        assertTrue("EF of K should be 14", graph.getEF(iterator.next().getElement()) == 14);
        assertTrue("EF of L should be 16", graph.getEF(iterator.next().getElement()) == 16);
        assertTrue("EF of FINISH should be 16", graph.getEF(iterator.next().getElement()) == 16);
    }

    /**
     * Test of getLS method, of class PertCpm.
     */
    @Test
    public void testGetLS() {
        System.out.println("testGetLS");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("LS of START should be 0", graph.getLS(iterator.next().getElement()) == 0);
        assertTrue("LS of A should be 2", graph.getLS(iterator.next().getElement()) == 2);
        assertTrue("LS of B should be 0", graph.getLS(iterator.next().getElement()) == 0);
        assertTrue("LS of C should be 4", graph.getLS(iterator.next().getElement()) == 4);
        assertTrue("LS of D should be 3", graph.getLS(iterator.next().getElement()) == 3);
        assertTrue("LS of E should be 8", graph.getLS(iterator.next().getElement()) == 8);
        assertTrue("LS of F should be 6", graph.getLS(iterator.next().getElement()) == 6);
        assertTrue("LS of G should be 10", graph.getLS(iterator.next().getElement()) == 10);
        assertTrue("LS of H should be 12", graph.getLS(iterator.next().getElement()) == 12);
        assertTrue("LS of I should be 13", graph.getLS(iterator.next().getElement()) == 13);
        assertTrue("LS of J should be 15", graph.getLS(iterator.next().getElement()) == 15);
        assertTrue("LS of K should be 13", graph.getLS(iterator.next().getElement()) == 13);
        assertTrue("LS of L should be 14", graph.getLS(iterator.next().getElement()) == 14);
        assertTrue("LS of FINISH should be 16", graph.getLS(iterator.next().getElement()) == 16);
    }

    /**
     * Test of getLF method, of class PertCpm.
     */
    @Test
    public void testGetLF() {
        System.out.println("testGetLF");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("LF of START should be 0", graph.getLF(iterator.next().getElement()) == 0);
        assertTrue("LF of A should be 3", graph.getLF(iterator.next().getElement()) == 3);
        assertTrue("LF of B should be 4", graph.getLF(iterator.next().getElement()) == 4);
        assertTrue("LF of C should be 6", graph.getLF(iterator.next().getElement()) == 6);
        assertTrue("LF of D should be 6", graph.getLF(iterator.next().getElement()) == 6);
        assertTrue("LF of E should be 10", graph.getLF(iterator.next().getElement()) == 10);
        assertTrue("LF of F should be 10", graph.getLF(iterator.next().getElement()) == 10);
        assertTrue("LF of G should be 13", graph.getLF(iterator.next().getElement()) == 13);
        assertTrue("LF of H should be 14", graph.getLF(iterator.next().getElement()) == 14);
        assertTrue("LF of I should be 14", graph.getLF(iterator.next().getElement()) == 14);
        assertTrue("LF of J should be 16", graph.getLF(iterator.next().getElement()) == 16);
        assertTrue("LF of K should be 14", graph.getLF(iterator.next().getElement()) == 14);
        assertTrue("LF of L should be 16", graph.getLF(iterator.next().getElement()) == 16);
        assertTrue("LF of FINISH should be 16", graph.getLF(iterator.next().getElement()) == 16);
    }

    /**
     * Test of getSlack method, of class PertCpm.
     */
    @Test
    public void testGetSlack() {
        System.out.println("testGetSlack");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("Slack of START should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of A should be 2", graph.getSLACK(iterator.next().getElement()) == 2);
        assertTrue("Slack of B should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of C should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of D should be 2", graph.getSLACK(iterator.next().getElement()) == 2);
        assertTrue("Slack of E should be 4", graph.getSLACK(iterator.next().getElement()) == 4);
        assertTrue("Slack of F should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of G should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of H should be 2", graph.getSLACK(iterator.next().getElement()) == 2);
        assertTrue("Slack of I should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of J should be 2", graph.getSLACK(iterator.next().getElement()) == 2);
        assertTrue("Slack of K should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of L should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
        assertTrue("Slack of FINISH should be 0", graph.getSLACK(iterator.next().getElement()) == 0);
    }

    /**
     * Test of getCriticalPaths method, of class PertCpm.
     */
    @Test
    public void testGetCriticalPaths() {
        System.out.println("getCriticalPaths");
        PertCpm graph = createGraph();
        List<Deque<Activity>> criticalPaths = graph.getCriticalPaths();
        assertTrue("There should be 2 paths", criticalPaths.size() == 2);
    }

    /**
     * Test of getAllPaths method, of class PertCpm.
     */
    @Test
    public void testGetAllPaths() {
        System.out.println("getAllPaths");
        PertCpm graph = createGraph();
        Deque<PertCpm.Path> allPaths = graph.getAllPaths();
        assertTrue("There should have been 11 paths", allPaths.size() == 11);

        Iterator<PertCpm.Path> iterator = allPaths.iterator();
        while(iterator.hasNext()){
            assertTrue("First activity should be START", iterator.next().getPath().peekFirst().getActivKey().equals("START"));
        }
    }

    /**
     * Test of getEs method, of class PertCpm.
     */
    @Test
    public void testGetEs() {
        System.out.println("getEs");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("should be 0",graph.getES(iterator.next().getElement())==0);
        assertTrue("should be 0",graph.getES(iterator.next().getElement())==0);
        assertTrue("should be 0",graph.getES(iterator.next().getElement())==0);
        assertTrue("should be 4",graph.getES(iterator.next().getElement())==4);
        assertTrue("should be 1",graph.getES(iterator.next().getElement())==1);
        assertTrue("should be 4",graph.getES(iterator.next().getElement())==4);
        assertTrue("should be 6",graph.getES(iterator.next().getElement())==6);
        assertTrue("should be 10",graph.getES(iterator.next().getElement())==10);
        assertTrue("should be 10",graph.getES(iterator.next().getElement())==10);
        assertTrue("should be 13",graph.getES(iterator.next().getElement())==13);
        assertTrue("should be 13",graph.getES(iterator.next().getElement())==13);
        assertTrue("should be 13",graph.getES(iterator.next().getElement())==13);
        assertTrue("should be 14",graph.getES(iterator.next().getElement())==14);
        assertTrue("should be 16",graph.getES(iterator.next().getElement())==16);
    }

    /**
     * Test of getEf method, of class PertCpm.
     */
    @Test
    public void testGetEf() {
        System.out.println("getEf");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("should be 0",graph.getEF(iterator.next().getElement())==0);
        assertTrue("should be 1",graph.getEF(iterator.next().getElement())==1);
        assertTrue("should be 4",graph.getEF(iterator.next().getElement())==4);
        assertTrue("should be 6",graph.getEF(iterator.next().getElement())==6);
        assertTrue("should be 4",graph.getEF(iterator.next().getElement())==4);
        assertTrue("should be 6",graph.getEF(iterator.next().getElement())==6);
        assertTrue("should be 10",graph.getEF(iterator.next().getElement())==10);
        assertTrue("should be 13",graph.getEF(iterator.next().getElement())==13);
        assertTrue("should be 12",graph.getEF(iterator.next().getElement())==12);
        assertTrue("should be 14",graph.getEF(iterator.next().getElement())==14);
        assertTrue("should be 14",graph.getEF(iterator.next().getElement())==14);
        assertTrue("should be 14",graph.getEF(iterator.next().getElement())==14);
        assertTrue("should be 16",graph.getEF(iterator.next().getElement())==16);
        assertTrue("should be 16",graph.getEF(iterator.next().getElement())==16);
    }

    /**
     * Test of getLs method, of class PertCpm.
     */
    @Test
    public void testGetLs() {
        System.out.println("getLs");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("should be 0",graph.getLS(iterator.next().getElement())==0);
        assertTrue("should be 2",graph.getLS(iterator.next().getElement())==2);
        assertTrue("should be 0",graph.getLS(iterator.next().getElement())==0);
        assertTrue("should be 4",graph.getLS(iterator.next().getElement())==4);
        assertTrue("should be 3",graph.getLS(iterator.next().getElement())==3);
        assertTrue("should be 8",graph.getLS(iterator.next().getElement())==8);
        assertTrue("should be 6",graph.getLS(iterator.next().getElement())==6);
        assertTrue("should be 10",graph.getLS(iterator.next().getElement())==10);
        assertTrue("should be 12",graph.getLS(iterator.next().getElement())==12);
        assertTrue("should be 13",graph.getLS(iterator.next().getElement())==13);
        assertTrue("should be 15",graph.getLS(iterator.next().getElement())==15);
        assertTrue("should be 13",graph.getLS(iterator.next().getElement())==13);
        assertTrue("should be 14",graph.getLS(iterator.next().getElement())==14);
        assertTrue("should be 16",graph.getLS(iterator.next().getElement())==16);
    }

    /**
     * Test of getLf method, of class PertCpm.
     */
    @Test
    public void testGetLf() {
        System.out.println("getLf");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("should be 0",graph.getLF(iterator.next().getElement())==0);
        assertTrue("should be 3",graph.getLF(iterator.next().getElement())==3);
        assertTrue("should be 4",graph.getLF(iterator.next().getElement())==4);
        assertTrue("should be 6",graph.getLF(iterator.next().getElement())==6);
        assertTrue("should be 6",graph.getLF(iterator.next().getElement())==6);
        assertTrue("should be 10",graph.getLF(iterator.next().getElement())==10);
        assertTrue("should be 10",graph.getLF(iterator.next().getElement())==10);
        assertTrue("should be 13",graph.getLF(iterator.next().getElement())==13);
        assertTrue("should be 14",graph.getLF(iterator.next().getElement())==14);
        assertTrue("should be 14",graph.getLF(iterator.next().getElement())==14);
        assertTrue("should be 16",graph.getLF(iterator.next().getElement())==16);
        assertTrue("should be 14",graph.getLF(iterator.next().getElement())==14);
        assertTrue("should be 16",graph.getLF(iterator.next().getElement())==16);
        assertTrue("should be 16",graph.getLF(iterator.next().getElement())==16);
        
    }

    /**
     * Test of getCost method, of class PertCpm.
     */
    @Test
    public void testGetCost() {
        System.out.println("getCost");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        iterator.next();
        assertTrue("A should be VCA 3360.0 €", graph.getCost(iterator.next().getElement()).equals("VCA 3360.0 €"));
        assertTrue("B should be FCA 2500.0 €", graph.getCost(iterator.next().getElement()).equals("FCA 2500.0 €"));
        assertTrue("C should be FCA 1250.0 €", graph.getCost(iterator.next().getElement()).equals("FCA 1250.0 €"));
        assertTrue("D should be VCA 4860.0 €", graph.getCost(iterator.next().getElement()).equals("VCA 4860.0 €"));
        assertTrue("E should be VCA 3240.0 €", graph.getCost(iterator.next().getElement()).equals("VCA 3240.0 €"));
        assertTrue("F should be VCA 2160.0 €", graph.getCost(iterator.next().getElement()).equals("VCA 2160.0 €"));
        assertTrue("G should be VCA 1080.0 €", graph.getCost(iterator.next().getElement()).equals("VCA 1080.0 €"));
        assertTrue("H should be VCA 1620.0 €", graph.getCost(iterator.next().getElement()).equals("VCA 1620.0 €"));
        assertTrue("I should be VCA 810.0 €", graph.getCost(iterator.next().getElement()).equals("VCA 810.0 €"));
        assertTrue("J should be FCA 550.0 €", graph.getCost(iterator.next().getElement()).equals("FCA 550.0 €"));
        assertTrue("K should be FCA 750.0 €", graph.getCost(iterator.next().getElement()).equals("FCA 750.0 €"));
        assertTrue("L should be FCA 1500.0 €", graph.getCost(iterator.next().getElement()).equals("FCA 1500.0 €"));
        
    }

    /**
     * Test of getSLACK method, of class PertCpm.
     */
    @Test
    public void testGetSLACK() {
        System.out.println("getSLACK");
        PertCpm graph = createGraph();
        Iterator<Vertex<Activity, String>> iterator = graph.vertices().iterator();
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 2",graph.getSLACK(iterator.next().getElement())==2);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 2",graph.getSLACK(iterator.next().getElement())==2);
        assertTrue("should be 4",graph.getSLACK(iterator.next().getElement())==4);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 2",graph.getSLACK(iterator.next().getElement())==2);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 2",graph.getSLACK(iterator.next().getElement())==2);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
        assertTrue("should be 0",graph.getSLACK(iterator.next().getElement())==0);
    }

    /**
     * Test of getEsArray method, of class PertCpm.
     */
    @Test
    public void testGetEsArray() {
        System.out.println("getEsArray");
        PertCpm graph = createGraph();
        int aux[] = {0,0,0,4,1,4,6,10,10,13,13,13,14,16};
        assertTrue("Es is incorrect",Arrays.equals(graph.getEsArray(), aux));
    }

    /**
     * Test of getEfArray method, of class PertCpm.
     */
    @Test
    public void testGetEfArray() {
        System.out.println("getEfArray");
        PertCpm graph = createGraph();
        int aux[] = {0,1,4,6,4,6,10,13,12,14,14,14,16,16};
        assertTrue("Ef is incorrect",Arrays.equals(graph.getEfArray(), aux));
    }

    /**
     * Test of getLsArray method, of class PertCpm.
     */
    @Test
    public void testGetLsArray() {
        System.out.println("getLsArray");
        PertCpm graph = createGraph();
        int aux[] = {0,2,0,4,3,8,6,10,12,13,15,13,14,16};
        assertTrue("Ls is incorrect",Arrays.equals(graph.getLsArray(), aux));
    }

    /**
     * Test of getLfArray method, of class PertCpm.
     */
    @Test
    public void testGetLfArray() {
        System.out.println("getLfArray");
        PertCpm graph = createGraph();
        int aux[] = {0,3,4,6,6,10,10,13,14,14,16,14,16,16};
        assertTrue("Lf is incorrect",Arrays.equals(graph.getLfArray(), aux));
    }

    /**
     * Test of getSlackArray method, of class PertCpm.
     */
    @Test
    public void testGetSlackArray() {
        System.out.println("getSlackArray");
        PertCpm graph = createGraph();
        int aux[] = {0,2,0,0,2,4,0,0,2,0,2,0,0,0};
        assertTrue("Slack is incorrect",Arrays.equals(graph.getSlackArray(), aux));
    }

}
