package search_tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import projeto_esinf.Project;

/**
 *
 * @author David
 */
public class TreeDelaysTest {
    
    private static Project p1,p2,p3,p4;
    
    public TreeDelaysTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
         p1 = new Project("1", new ArrayList<>(), "Empresa1", 0, 175);
         p2 = new Project("2", new ArrayList<>(), "Empresa2", 0, 127);
         p3 = new Project("3", new ArrayList<>(), "Empresa3", 0, 593);
         p4 = new Project("4", new ArrayList<>(), "Empresa4", 0, 593);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insert method, of class TreeDelays.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        TreeDelays<Project> tree = new TreeDelays<>();
        tree.insertActivity(p1);
        tree.insertActivity(p2);
        tree.insertActivity(p3);
        tree.insertActivity(p4);
        
        List<Integer> projectsInOrder = new ArrayList<>(Arrays.asList(127, 175, 593)); 
        Iterator<TreeDelays.Ocorrences<Project>> iterator = tree.inOrder().iterator();
        int i=0;
        while(iterator.hasNext()){
            assertEquals(projectsInOrder.get(i), (Integer)iterator.next().getDelay());
            i++;
        }
        assertEquals(projectsInOrder.size(), i);
        
        List<String> projectInOrderString = new ArrayList<>(Arrays.asList("2 ", "1 ", "3 4 "));
        iterator = tree.inOrder().iterator();
        i=0;
        while(iterator.hasNext()){
            assertEquals("project id: "+i,projectInOrderString.get(i),iterator.next().getOcorrences());
            i++;
        }
        assertEquals(projectInOrderString.size(), i);
        
    }
    
}
