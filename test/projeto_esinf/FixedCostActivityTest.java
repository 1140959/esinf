/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_esinf;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *@author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class FixedCostActivityTest {

    public FixedCostActivityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getActivityCost method, of class FixedCostActivity.
     */
    @Test
    public void testGetActivityCost() {
        System.out.println("getActivityCost");
        FixedCostActivity instance = new FixedCostActivity();
        assertTrue("The Activity Cost should be 0.0:", instance.getActivityCost() == 0.0);
    }

    /**
     * Test of setActivityCost method, of class FixedCostActivity.
     */
    @Test
    public void testSetActivityCost() {
        System.out.println("setActivityCost");
        FixedCostActivity instance = new FixedCostActivity();
        instance.setActivityCost(1.0);
        assertTrue("The Activity Cost should be 1.0:", instance.getActivityCost() == 1.0);
    }

    /**
     * Test of toString method, of class FixedCostActivity.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        FixedCostActivity instance = new FixedCostActivity();
        String expResult = "Activity Key: " + "NA"
                + "\nActivity Type: " + "NA"
                + "\nDescription: " + "NA"
                + "\nDuration: " + 0
                + "\nDuration Unit: " + "hour"
                + "\nActivity Previous: "
                + "\nActivity Cost: " + 0.0;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class FixedCostActivity.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        FixedCostActivity instance = new FixedCostActivity();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
