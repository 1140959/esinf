/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_esinf;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *@author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class ActivityTest {

    public ActivityTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getActivKey method, of class Activity.
     */
    @Test
    public void testGetActivKey() {
        System.out.println("getActivKey");
        Activity instance = new ActivityImpl();
        assertTrue("The Activity key should be NA:", instance.getActivKey().equals("NA"));
    }

    /**
     * Test of getActivType method, of class Activity.
     */
    @Test
    public void testGetActivType() {
        System.out.println("getActivType");
        Activity instance = new ActivityImpl();
        assertTrue("The Activity type should be NA:", instance.getActivType().equals("NA"));
    }

    /**
     * Test of getDescription method, of class Activity.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Activity instance = new ActivityImpl();
        String expResult = "NA";
        assertTrue("The Activity description should be NA:", instance.getDescription().equals("NA"));
    }

    /**
     * Test of getDuration method, of class Activity.
     */
    @Test
    public void testGetDuration() {
        System.out.println("getDuration");
        Activity instance = new ActivityImpl();
        assertTrue("The Activity duration should be NA:", instance.getDuration() == 0);
    }

    /**
     * Test of getDurationUnit method, of class Activity.
     */
    @Test
    public void testGetDurationUnit() {
        System.out.println("getDurationUnit");
        Activity instance = new ActivityImpl();
        assertTrue("The Activity duration unit should be hour:", instance.getDurationUnit().equals("hour"));
    }

    /**
     * Test of getActivPrev method, of class Activity.
     */
    @Test
    public void testGetActivPrev() {
        System.out.println("getActivPrev");
        Activity instance = new ActivityImpl();
        assertTrue("The Activity Prev unit should be null:", instance.getActivPrev().isEmpty());
    }

    /**
     * Test of setActivKey method, of class Activity.
     */
    @Test
    public void testSetActivKey() {
        System.out.println("setActivKey");
        String activKey = "Key";
        Activity instance = new ActivityImpl();
        instance.setActivKey(activKey);
        assertTrue("The Activity key should be Key:", instance.getActivKey().equals("Key"));
    }

    /**
     * Test of setActivType method, of class Activity.
     */
    @Test
    public void testSetActivType() {
        System.out.println("setActivType");
        String activType = "Type";
        Activity instance = new ActivityImpl();
        instance.setActivType(activType);
        assertTrue("The Activity type should be Type:", instance.getActivType().equals("Type"));
    }

    /**
     * Test of setDescription method, of class Activity.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "Description";
        Activity instance = new ActivityImpl();
        instance.setDescription(description);
        assertTrue("The Activity Description should be Description:", instance.getDescription().equals("Description"));
    }

    /**
     * Test of setDuration method, of class Activity.
     */
    @Test
    public void testSetDuration() {
        System.out.println("setDuration");
        int duration = 1;
        Activity instance = new ActivityImpl();
        instance.setDuration(duration);
        assertTrue("The Activity Duration should be 1:", instance.getDuration() == 1);
    }

    /**
     * Test of setDurationUnit method, of class Activity.
     */
    @Test
    public void testSetDurationUnit() {
        System.out.println("setDurationUnit");
        String durationUnit = "day";
        Activity instance = new ActivityImpl();
        instance.setDurationUnit(durationUnit);
        assertTrue("The Activity duration unit should be day:", instance.getDurationUnit().equals("day"));
    }

    /**
     * Test of setActivPrev method, of class Activity.
     */
    @Test
    public void testSetActivPrev() {
        System.out.println("setActivPrev");
        List<String> activPrev = new ArrayList<>();
        activPrev.add("Prev1");
        activPrev.add("Prev2");
        Activity instance = new ActivityImpl();
        instance.setActivPrev(activPrev);
        assertTrue("The Activity Prev should be Prev1:", instance.getActivPrev().get(0).equals("Prev1"));
        assertTrue("The Activity Prev should be Prev2:", instance.getActivPrev().get(1).equals("Prev2"));
    }

    /**
     * Test of toString method, of class Activity.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Activity instance = new ActivityImpl();
        String expResult = "Activity Key: " + "NA"
                + "\nActivity Type: " + "NA"
                + "\nDescription: " + "NA"
                + "\nDuration: " + 0
                + "\nDuration Unit: " + "hour"
                + "\nActivity Previous: ";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of addActivPrev method, of class Activity.
     */
    @Test
    public void testAddActivPrev() {
        System.out.println("addActivPrev");
        Activity instance = new ActivityImpl();
        instance.addActivPrev("Prev1");
        assertTrue("The Activity Prev should be Prev1:", instance.getActivPrev().get(0).equals("Prev1"));
    }

    /**
     * Test of addActivPrevAt method, of class Activity.
     */
    @Test
    public void testAddActivPrevAt() {
        System.out.println("addActivPrevAt");
        int position = 0;
        String activPrev = "Prev1";
        Activity instance = new ActivityImpl();
        instance.addActivPrevAt(position, activPrev);
        assertTrue("The Activity Prev At should be Prev1:", instance.getActivPrev().get(0).equals("Prev1"));
    }

    /**
     * Test of removeActivPrev method, of class Activity.
     */
    @Test
    public void testRemoveActivPrev() {
        System.out.println("removeActivPrev");
        Activity instance = new ActivityImpl();
        instance.addActivPrev("Prev1");
        instance.addActivPrev("Prev2");
        instance.removeActivPrev("Prev1");
        assertTrue("Size should be 1:", instance.getActivPrev().size() == 1);
        assertTrue("The Activity Prev should be Prev2:", instance.getActivPrev().get(0).equals("Prev2"));
    }

    /**
     * Test of removeActivPrevAt method, of class Activity.
     */
    @Test
    public void testRemoveActivPrevAt() {
        System.out.println("removeActivPrevAt");
        int position = 1;
        Activity instance = new ActivityImpl();
        instance.addActivPrev("Prev1");
        instance.addActivPrev("Prev2");
        instance.removeActivPrevAt(position);
        assertTrue("Size should be 1:", instance.getActivPrev().size() == 1);
        assertTrue("The Activity Prev should be Prev1:", instance.getActivPrev().get(0).equals("Prev1"));
    }

    /**
     * Test of equals method, of class Activity.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Activity instance = new ActivityImpl();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    public class ActivityImpl extends Activity {

        @Override
        public int compareTo(Activity o) {
            throw new UnsupportedOperationException("Not supported yet."); 
        }
    }

}
