/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_esinf;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *@author 1140959@isep.ipp.pt
 * @author 1140858@isep.ipp.pt
 */
public class ProjectTest {
    
    private static Project createProj() {
        List<String> prev = new ArrayList<>();
        List<Activity> activities = new ArrayList();
        activities.add(new VariableCostActivity(112,30, "A", "VCA", "a", 1, "a", new ArrayList<>()));
        activities.add(new FixedCostActivity(2500, "B", "FCA", null, 4, null, new ArrayList<>()));

        prev.clear();
        prev.add("B");
        activities.add(new FixedCostActivity(1250, "C", "VCA", null, 2, null, prev));

        prev.clear();
        prev.add("A");
        activities.add(new VariableCostActivity(162,30, "D", "VCA", null, 3, null, prev));

        prev.clear();
        prev.add("D");
        activities.add(new VariableCostActivity(108,30, "E", "VCA", null, 2, null, prev));

        prev.clear();
        prev.add("C");
        prev.add("D");
        activities.add(new VariableCostActivity(108,20, "F", "VCA", null, 4, null, prev));

        prev.clear();
        prev.add("E");
        prev.add("F");
        activities.add(new VariableCostActivity(54,20, "G", "VCA", null, 3, null, prev));

        prev.clear();
        prev.add("F");
        activities.add(new VariableCostActivity(54,30, "H", "VCA", null, 2, null, prev));

        prev.clear();
        prev.add("G");
        activities.add(new VariableCostActivity(27,30, "I", "VCA", null, 1, null, prev));

        prev.clear();
        prev.add("G");
        activities.add(new FixedCostActivity(550, "J", "FCA", null, 1, null, prev));

        prev.clear();
        prev.add("G");
        activities.add(new FixedCostActivity(750, "K", "FCA", null, 1, null, prev));

        prev.clear();
        prev.add("H");
        prev.add("I");
        prev.add("K");
        activities.add(new FixedCostActivity(1500, "L", "FCA", null, 2, null, prev));

        return new Project("1", activities,"N/A",0,0);
    }
    
    public ProjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Project.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Project proj = createProj();
        assertTrue("id shoud be 1",proj.getId().equals("1"));
    }

    /**
     * Test of setId method, of class Project.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Project proj = createProj();
        proj.setId("2");
        assertTrue("id shoud be 2",proj.getId().equals("2"));
    }

    /**
     * Test of getActiviesList method, of class Project.
     */
    @Test
    public void testGetActiviesList() {
        System.out.println("getActiviesList");
        Project proj = createProj();
        List<Activity> activiesList = proj.getActiviesList();
        assertTrue("Should have 12 activities",activiesList.size()==12);
        assertTrue("act 1 should be A",activiesList.get(0).getActivKey().equals("A"));
        assertTrue("act 2 should be B",activiesList.get(1).getActivKey().equals("B"));
        assertTrue("act 3 should be C",activiesList.get(2).getActivKey().equals("C"));
        assertTrue("act 4 should be D",activiesList.get(3).getActivKey().equals("D"));
        assertTrue("act 5 should be E",activiesList.get(4).getActivKey().equals("E"));
        assertTrue("act 6 should be F",activiesList.get(5).getActivKey().equals("F"));
        assertTrue("act 7 should be G",activiesList.get(6).getActivKey().equals("G"));
        assertTrue("act 8 should be H",activiesList.get(7).getActivKey().equals("H"));
        assertTrue("act 9 ould be I",activiesList.get(8).getActivKey().equals("I"));
        assertTrue("act 10 should be J",activiesList.get(9).getActivKey().equals("J"));
        assertTrue("act 11 should be K",activiesList.get(10).getActivKey().equals("K"));
        assertTrue("act 12 should be L",activiesList.get(11).getActivKey().equals("L"));

    }

    /**
     * Test of setActiviesList method, of class Project.
     */
    @Test
    public void testSetActiviesList() {
        System.out.println("setActiviesList");
        List<String> prev = new ArrayList<>();
        List<Activity> activities = new ArrayList();
        prev.clear();
        prev.add("G");
        activities.add(new FixedCostActivity(550, "J", "FCA", null, 1, null, prev));

        prev.clear();
        prev.add("G");
        activities.add(new FixedCostActivity(750, "K", "FCA", null, 1, null, prev));

        prev.clear();
        prev.add("H");
        prev.add("I");
        prev.add("K");
        activities.add(new FixedCostActivity(1500, "L", "FCA", null, 2, null, prev));
        Project proj = createProj();
        proj.setActiviesList(activities);
        
        List<Activity> activiesList = proj.getActiviesList();
  
        assertTrue("Should have 3 activities",activiesList.size()==3);
        assertTrue("act 1 should be J",activiesList.get(0).getActivKey().equals("J"));
        assertTrue("act 2 should be K",activiesList.get(1).getActivKey().equals("K"));
        assertTrue("act 3 should be L",activiesList.get(2).getActivKey().equals("L"));
        
        
    }

    /**
     * Test of getActivityByKey method, of class Project.
     */
    @Test
    public void testGetActivityByKey() {
        System.out.println("getActivityByKey");
        Project proj = createProj();
        Activity activityByKey = proj.getActivityByKey("A");
        assertTrue("activity shoud be A",activityByKey.getActivKey().equals("A"));
        activityByKey = proj.getActivityByKey("B");
        assertTrue("activity shoud be B",activityByKey.getActivKey().equals("B"));
        activityByKey = proj.getActivityByKey("C");
        assertTrue("activity shoud be C",activityByKey.getActivKey().equals("C"));
        activityByKey = proj.getActivityByKey("D");
        assertTrue("activity shoud be D",activityByKey.getActivKey().equals("D"));
        activityByKey = proj.getActivityByKey("E");
        assertTrue("activity shoud be E",activityByKey.getActivKey().equals("E"));
    }

    /**
     * Test of addActiv method, of class Project.
     */
    @Test
    public void testAddActiv() {
        System.out.println("addActiv");
        Project proj = createProj();
        proj.addActiv(new FixedCostActivity(550, "J", "FCA", null, 1, null, new ArrayList<>()));
        List<Activity> activiesList = proj.getActiviesList();
        assertTrue("new size should be 13 ",activiesList.size()==13);
        proj.addActiv(new FixedCostActivity(550, "J", "FCA", null, 1, null, new ArrayList<>()));
        activiesList = proj.getActiviesList();
        assertTrue("new size should be 14 ",activiesList.size()==14);
    }

    /**
     * Test of addActivAt method, of class Project.
     */
    @Test
    public void testAddActivAt() {
        System.out.println("addActivAt");
        Project proj = createProj();
        proj.addActivAt(1,new FixedCostActivity(550, "J", "FCA", null, 1, null, new ArrayList<>()));
        List<Activity> activiesList = proj.getActiviesList();
        assertTrue("new size should be 13 ",activiesList.size()==13);
        proj.addActivAt(5,new FixedCostActivity(550, "Z", "FCA", null, 1, null, new ArrayList<>()));
        activiesList = proj.getActiviesList();
        assertTrue("new size should be 14 ",activiesList.size()==14);
        assertTrue("Activ 1 should be J",activiesList.get(1).getActivKey().equals("J"));
        assertTrue("Activ 5 should be Z",activiesList.get(5).getActivKey().equals("Z"));
    }

    /**
     * Test of removeActiv method, of class Project.
     */
    @Test
    public void testRemoveActiv() {
        System.out.println("removeActiv");
        Project proj = createProj();
        proj.removeActiv(new VariableCostActivity(112,30, "A", "VCA", "a", 1, "a", new ArrayList<>()));
        List<Activity> activiesList = proj.getActiviesList();
        assertTrue("new size should be 11",activiesList.size()==11);
        assertTrue("position 0 should be B now",activiesList.get(0).getActivKey().equals("B"));
    }

    /**
     * Test of removeActivAt method, of class Project.
     */
    @Test
    public void testRemoveActivAt() {
        System.out.println("removeActivAt");
        Project proj = createProj();
        proj.removeActivAt(0);
        List<Activity> activiesList = proj.getActiviesList();
        assertTrue("new size should be 11",activiesList.size()==11);
        assertTrue("position 0 should be B now",activiesList.get(0).getActivKey().equals("B"));
    }

}
