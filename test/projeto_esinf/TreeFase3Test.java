package projeto_esinf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import search_tree.TreeDelays.Ocorrences;

/**
 *
 * @author David
 */
public class TreeFase3Test {

    private static Project p2;
    private static Project p4;
    private static List<Project> lista;

    public TreeFase3Test() {

    }

    @BeforeClass
    public static void setUpClass() {
        List<Activity> list1 = new ArrayList<>();
        String idProject1 = "1";
        list1.add(new VariableCostActivity(idProject1, "A", "VCA", 8));
        list1.add(new VariableCostActivity(idProject1, "B", "VCA", 4));
        list1.add(new VariableCostActivity(idProject1, "C", "VCA", 2));
        list1.add(new VariableCostActivity(idProject1, "D", "VCA", 6));
        list1.add(new VariableCostActivity(idProject1, "E", "VCA", 3));
        list1.add(new FixedCostActivity(idProject1, "F", "FCA", 70));
        list1.add(new FixedCostActivity(idProject1, "G", "FCA", 12));
        list1.add(new FixedCostActivity(idProject1, "H", "FCA", 15));
        list1.add(new FixedCostActivity(idProject1, "I", "FCA", 30));
        list1.add(new FixedCostActivity(idProject1, "J", "FCA", 25));
        Project p1 = new Project(idProject1, list1, "Empresa1", 0, 175);
        
        //project 2
        List<Activity> list2 = new ArrayList<>();
        String idProject2 = "2";
        list2.add(new VariableCostActivity(idProject2, "K", "VCA", 4));
        list2.add(new VariableCostActivity(idProject2, "L", "VCA", 28));
        list2.add(new VariableCostActivity(idProject2, "M", "VCA", 6));
        list2.add(new VariableCostActivity(idProject2, "N", "VCA", 45));
        list2.add(new VariableCostActivity(idProject2, "O", "VCA", 5));
        list2.add(new FixedCostActivity(idProject2, "P", "FCA", 7));
        list2.add(new FixedCostActivity(idProject2, "Q", "FCA", 5));
        list2.add(new FixedCostActivity(idProject2, "R", "FCA", 6));
        list2.add(new FixedCostActivity(idProject2, "S", "FCA", 5));
        list2.add(new FixedCostActivity(idProject2, "T", "FCA", 16));
        p2 = new Project(idProject2, list2, "Empresa2", 0, 127);
        
        //project 3 e 4
        List<Activity> list3 = new ArrayList<>();
        String idProject3 = "3";
        list3.add(new VariableCostActivity(idProject3, "A", "VCA", 0));
        list3.add(new VariableCostActivity(idProject3, "B", "VCA", 7));
        list3.add(new VariableCostActivity(idProject3, "C", "VCA", 5));
        list3.add(new VariableCostActivity(idProject3, "D", "VCA", 85));
        list3.add(new VariableCostActivity(idProject3, "E", "VCA", 45));
        list3.add(new FixedCostActivity(idProject3, "F", "FCA", 12));
        list3.add(new FixedCostActivity(idProject3, "G", "FCA", 369));
        list3.add(new FixedCostActivity(idProject3, "H", "FCA", 5));
        list3.add(new FixedCostActivity(idProject3, "I", "FCA", 7));
        list3.add(new FixedCostActivity(idProject3, "J", "FCA", 58));
        Project p3 = new Project(idProject3, list3, "Empresa3", 0, 593);

        List<Activity> list4 = new ArrayList<>();
        String idProject4 = "4";
        list4.add(new VariableCostActivity(idProject4, "A", "VCA", 0));
        list4.add(new VariableCostActivity(idProject4, "B", "VCA", 7));
        list4.add(new VariableCostActivity(idProject4, "C", "VCA", 5));
        list4.add(new VariableCostActivity(idProject4, "D", "VCA", 85));
        list4.add(new VariableCostActivity(idProject4, "E", "VCA", 45));
        list4.add(new FixedCostActivity(idProject4, "F", "FCA", 12));
        list4.add(new FixedCostActivity(idProject4, "G", "FCA", 369));
        list4.add(new FixedCostActivity(idProject4, "H", "FCA", 5));
        list4.add(new FixedCostActivity(idProject4, "I", "FCA", 7));
        list4.add(new FixedCostActivity(idProject4, "J", "FCA", 58));
        p4 = new Project(idProject4, list4, "Empresa4", 0, 593);
        
        // all
        lista = new ArrayList<>(Arrays.asList(p1, p2, p3, p4));
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testData() {
        System.out.println("data");
        TreeFase3 teste = new TreeFase3(lista);
//        System.out.println(teste.getProjectTree());
//        System.out.println(teste.getActivityTreeList());

        List<Integer> projectsInOrder = new ArrayList<>(Arrays.asList(127, 175, 593));
        List<Integer> activitiesInOrder = new ArrayList<>(Arrays.asList(0, 2, 3, 4, 5, 6, 7, 8, 12, 15, 16, 25, 28, 30, 45, 58, 70, 85, 369));
        List<String> projectInOrderString = new ArrayList<>(Arrays.asList("2 ", "1 ", "3 4 "));
        List<String> activitiesInOrderString = new ArrayList<>(Arrays.asList("3.A 4.A ", "1.C ", "1.E ", "1.B 2.K ", "2.O 2.Q 2.S 3.C 3.H 4.C 4.H ", "1.D 2.M 2.R ",
                "2.P 3.B 3.I 4.B 4.I ", "1.A ", "1.G 3.F 4.F ", "1.H ", "2.T ", "1.J ", "2.L ", "1.I ", "2.N 3.E 4.E ", "3.J 4.J ", "1.F ", "3.D 4.D ",
                "3.G 4.G "));

        Iterator<Ocorrences<Project>> inOrderProject = teste.getProjectTree().inOrder().iterator();
        int i = 0;
        while (inOrderProject.hasNext()) {
            Ocorrences<Project> next = inOrderProject.next();
            assertEquals("Activities of project by node", next.getOcorrences(), projectInOrderString.get(i));
            assertEquals("Delay of project", projectsInOrder.get(i), (Integer) next.getDelay());
            i++;
        }
        assertEquals("Number of nodes in project tree", projectsInOrder.size(), i);

        Iterator<Ocorrences<Activity>> inOrderActivity = teste.getActivityTreeList().inOrder().iterator();
        i = 0;
        while (inOrderActivity.hasNext()) {
            Ocorrences<Activity> next = inOrderActivity.next();
            assertEquals("Activities of project by node", next.getOcorrences(), activitiesInOrderString.get(i));
            assertEquals("Delay of all activities", activitiesInOrder.get(i), (Integer) next.getDelay());
            i++;
        }
        assertEquals("Numer of nodes in activity tree", activitiesInOrder.size(), i);
    }

    /**
     * Test of printProjectsByDelayTime method, of class TreeFase3.
     */
    @Test
    public void testPrintProjectsByDelayTime() {
        System.out.println("printProjectsByDelayTime");
        TreeFase3 teste = new TreeFase3(lista);
//        System.out.println(teste.printActivitiesByDelayTime());

        String expected = "3.A 4.A Delay:0\n"
                + "1.C Delay:2\n"
                + "1.E Delay:3\n"
                + "1.B 2.K Delay:4\n"
                + "2.O 2.Q 2.S 3.C 3.H 4.C 4.H Delay:5\n"
                + "1.D 2.M 2.R Delay:6\n"
                + "2.P 3.B 3.I 4.B 4.I Delay:7\n"
                + "1.A Delay:8\n"
                + "1.G 3.F 4.F Delay:12\n"
                + "1.H Delay:15\n"
                + "2.T Delay:16\n"
                + "1.J Delay:25\n"
                + "2.L Delay:28\n"
                + "1.I Delay:30\n"
                + "2.N 3.E 4.E Delay:45\n"
                + "3.J 4.J Delay:58\n"
                + "1.F Delay:70\n"
                + "3.D 4.D Delay:85\n"
                + "3.G 4.G Delay:369";
        
        assertTrue(expected.equals(teste.printActivitiesByDelayTime()));
    }

    /**
     * Test of lateActivitiesWithSameType method, of class TreeFase3.
     */
    @Test
    public void testLateActivitiesWithSameType() {
        System.out.println("lateActivitiesWithSameType");
        TreeFase3 teste = new TreeFase3(lista);
        
        List<String> vca = new ArrayList<>(Arrays.asList("2.K","2.O","4.C","2.M","4.B","2.L","2.N","4.E","4.D"));
        List<String> fca = new ArrayList<>(Arrays.asList("2.Q","2.S","4.H","2.R","2.P","4.I","4.F","2.T","4.J","4.G"));
        
        List<List<Activity>> lateActivitiesWithSameType = teste.lateActivitiesWithSameType(p2, p4);
        
        int i =0;
        for ( Activity actVCA : lateActivitiesWithSameType.get(0)) {
            assertEquals(vca.get(i), actVCA.getProjectId()+"."+actVCA.getActivKey());
            i++;
        }
        assertEquals("Size",vca.size(), i); 
        
        i=0;
        for (Activity actFCA : lateActivitiesWithSameType.get(1)) {
            assertEquals(fca.get(i), actFCA.getProjectId()+"."+actFCA.getActivKey());
            i++;
        }
        assertEquals("Size",fca.size(), i);
    }

}
